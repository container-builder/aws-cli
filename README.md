# AWS CLI Container Image Builder

[![Docker Repository on Quay](https://quay.io/repository/container-builder/aws-cli/status "Docker Repository on Quay")](https://quay.io/repository/container-builder/aws-cli)

This repository provides a Containerfile and GitLab CI/CD configuration to build an AWS Command Line Interface (CLI) container image using Podman and push it to Quay.io.

## Purpose

The purpose of this repository is to automate the building and pushing of an AWS CLI container image into a container registry. By using GitLab CI/CD and Podman.

## How it Works

1. The Containerfile in this repository sets up an Alpine Linux-based image and installs the AWS CLI, boto3, and py3-apipkg packages.

2. The GitLab CI/CD configuration file (`.gitlab-ci.yml`) defines a pipeline that builds the AWS CLI container image and pushes it to Quay.io.

3. During the pipeline execution, the image is built using Podman and tagged accordingly.

4. The built image is then pushed to the specified Quay.io registry, ensuring it is available for deployment or further usage.


## Contributing

Contributions are welcome! If you encounter any issues or have suggestions for improvement, please feel free to open an issue or submit a pull request to this repository.

## License

This repository is licensed under the [MIT License](LICENSE).

